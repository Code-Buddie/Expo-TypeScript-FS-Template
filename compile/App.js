var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
export default function App() {
    return (<View style={styles.container}>
      <TSComponent />
    </View>);
}
var TSComponent = /** @class */ (function (_super) {
    __extends(TSComponent, _super);
    function TSComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TSComponent.prototype.typeCheckTest = function (variable) {
        console.log(variable);
    };
    TSComponent.prototype.render = function () {
        this.typeCheckTest('123'); // try altering the argument's type!
        return (<View>
        <Text>Hey there, fellow developer!</Text>
      </View>);
    };
    return TSComponent;
}(React.Component));
var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
