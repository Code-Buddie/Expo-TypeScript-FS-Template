# Expo-TypeScript-FS-Template
A fail-safe TypeScript template for Expo SDK 35 projects.

## Usage
### Starting the Expo server

`yarn start`

__NOTE:__ Do not try to use `expo start`. This will start Metro Bundler but it will not start the TypeScript compiler that's necessary to compile your code.

### Writing code
All of your TypeScript goes in `src`; treat this as your top-level directory for TypeScript source. 

The resulting compiled JavaScript (which you shouldn't touch) is located in `compile`.

## Another Template?
Yes, but this one's necessary. Expo's blank TypeScript template does a fine job of allowing you to develop apps in TypeScript right out of the box, but where it's not so good is enforcing TypeScript itself.

Expo will compile your TypeScript code and hot reload it into your experience even if there are glaring type errors in it, introducing the very runtime errors we're aiming to avoid by using TypeScript in the first place.

This template was developed to stop error-producing TypeScript compilations from emitting JavaScript, in turn keeping Metro Bundler from hot reloading faulty code into your app.

## Why is this template primarily JavaScript?
Expo compiles your TypeScript in the cloud; placing a gatekeeper between Expo's TypeScript compilation and JavaScript bundling requires that we do this compilation locally instead. Of course, this means that we'll end up with more JavaScript code than TypeScript code.

Don't worry though, this template fully expects the code you write to be pure TypeScript. :)

## Isn't this slower than Expo's normal TypeScript compile?
Yes, but this is only noticeable for the first build of your JavaScript bundle after starting the Expo server. TypeScript incrementally compiles new code changes when running in watch mode, so the difference in reload time should be negligible.
