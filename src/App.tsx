import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <TSComponent />
    </View>
  );
}

class TSComponent extends React.Component {
  typeCheckTest(variable: string) {
    console.log(variable);
  }

  render() {
    this.typeCheckTest('123');      // try altering the argument's type!
    return (
      <View>
        <Text>Hey there, fellow developer!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});